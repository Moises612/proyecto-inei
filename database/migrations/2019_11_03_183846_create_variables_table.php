<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVariablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('variables', function (Blueprint $table) {
            $table->bigIncrements('n_codvar');
            $table->bigInteger('n_codsva')->unsigned()->index();
            $table->string('c_desvar',150);
            $table->string('c_estado',1)->default('S');
            $table->foreign('n_codsva')
                    ->references('n_codsva')
                    ->on('sector_variables');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('variables', function (Blueprint $table) {
            $table->dropForeign(['n_codsva']);
        });
        Schema::dropIfExists('variables');
    }
}
