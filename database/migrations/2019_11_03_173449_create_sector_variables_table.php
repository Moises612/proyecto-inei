<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSectorVariablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sector_variables', function (Blueprint $table) {
            $table->bigIncrements('n_codsva');
            $table->bigInteger('n_codsec')->unsigned()->index();
            $table->string('c_desvar',150);
            $table->string('c_estado',1)->default('S');
            $table->foreign('n_codsec')
                        ->references('n_codsec')
                        ->on('sectors');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sector_variables', function (Blueprint $table) {
            $table->dropForeign(['n_codsec']);
        });
        Schema::dropIfExists('sector_variables');
    }
}
