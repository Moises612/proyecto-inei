<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUbigeosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ubigeos', function (Blueprint $table) {
            //$table->bigIncrements('id');
            $table->string('c_codubi')->primary();
            $table->string('c_coddep',2);
            $table->string('c_desdep',150);
            $table->string('c_codpro',2);
            $table->string('c_despro',150);
            $table->string('c_coddis',2);
            $table->string('c_desdis',150);
            $table->string('c_codcpo',4);
            $table->string('c_descpo',150);
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ubigeos');
    }
}
