<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResumensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resumens', function (Blueprint $table) {
            $table->bigInteger('n_codfue')->unsigned()->index();
            $table->string('c_codubi',10)->index();
            $table->bigInteger('n_codsec')->unsigned()->index();
            $table->bigInteger('n_codsva')->unsigned()->index();
            $table->bigInteger('n_codvar')->unsigned()->index();
            $table->integer('n_valor');
            $table->foreign('n_codfue')->references('n_codfue')->on('fuentes');
            $table->foreign('c_codubi')->references('c_codubi')->on('ubigeos');
            $table->foreign('n_codsec')->references('n_codsec')->on('sectors');
            $table->foreign('n_codsva')->references('n_codsva')->on('sector_variables');
            $table->foreign('n_codvar')->references('n_codvar')->on('variables');
            $table->primary(['n_codfue', 'c_codubi','n_codvar']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('resumens', function (Blueprint $table) {
            $table->dropForeign(['n_codfue']);
            $table->dropForeign(['c_codubi']);
            $table->dropForeign(['n_codsec']);
            $table->dropForeign(['n_codsva']);
            $table->dropForeign(['n_codvar']);
        });
        Schema::dropIfExists('resumens');
    }
}
