<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ubigeo extends Model
{
    
    protected $primaryKey = 'c_codubi';
    public $incrementing = false;
    protected $keyType = 'string';//indicar que la clave primaria no es un int

}
