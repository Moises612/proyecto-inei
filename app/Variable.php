<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Variable extends Model
{
    protected $primaryKey = 'n_codvar';

    public function sectorVariable(){
        return $this->belongsTo('App\SectorVariable');
    }
}
