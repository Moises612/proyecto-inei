<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fuente extends Model
{
    protected $primaryKey = 'n_codfue';
}
