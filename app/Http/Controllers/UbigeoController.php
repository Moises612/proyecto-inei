<?php

namespace App\Http\Controllers;

use App\Ubigeo;
use Illuminate\Http\Request;

class UbigeoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ubigeos = Ubigeo::all();
        return response()->json($ubigeos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ubigeo  $ubigeo
     * @return \Illuminate\Http\Response
     */
    public function show(Ubigeo $ubigeo)
    {
        $ubi= Ubigeo::where('c_codubi','=',$ubigeo->c_codubi)->first();
        return response()->json($ubi);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ubigeo  $ubigeo
     * @return \Illuminate\Http\Response
     */
    public function edit(Ubigeo $ubigeo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ubigeo  $ubigeo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ubigeo $ubigeo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ubigeo  $ubigeo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ubigeo $ubigeo)
    {
        //
    }
}
