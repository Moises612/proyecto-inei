<?php

namespace App\Http\Controllers;

use App\SectorVariable;
use Illuminate\Http\Request;

class SectorVariableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SectorVariable  $sectorVariable
     * @return \Illuminate\Http\Response
     */
    public function show(SectorVariable $sectorVariable)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SectorVariable  $sectorVariable
     * @return \Illuminate\Http\Response
     */
    public function edit(SectorVariable $sectorVariable)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SectorVariable  $sectorVariable
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SectorVariable $sectorVariable)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SectorVariable  $sectorVariable
     * @return \Illuminate\Http\Response
     */
    public function destroy(SectorVariable $sectorVariable)
    {
        //
    }
}
