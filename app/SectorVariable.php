<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SectorVariable extends Model
{
    protected $primaryKey = 'n_codsva';

    public function sector(){
        return $this->belongsTo('App\Sector');
    }

    public function variables(){
        return $this->hasMany('App\Variable');
    }
}
