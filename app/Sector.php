<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sector extends Model
{
    protected $primaryKey = 'n_codsec';

    public function sectorVariables(){
    	return $this->hasMany('App\SectorVariable');
    }
}
